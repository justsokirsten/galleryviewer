package com.kirsten.mobileappdev.galleryviewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {
	private static final String TAG = "Images";
	private Context mContext;
	private ArrayList<ImageData> imageList;
	private LruCache<String, Bitmap> memoryCache;

	ImageAdapter(Context c, ArrayList<ImageData> images, LruCache<String, Bitmap> mMemoryCache) {
		mContext = c;
		imageList = images;
		memoryCache = mMemoryCache;
	}

	class ViewHolder{
		int position;
		ImageView imageView;
	}

	public int getCount(){
		return imageList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// Add bitmap to cache if it doesn't exist already
	public void addBitmapToMemoryCache(String key, Bitmap bmp) {
		if (getBitmapFromMemCache(key) == null) {
			memoryCache.put(key, bmp);
		}
	}

	// Retrieve bitmap from cache
	public Bitmap getBitmapFromMemCache(String key) {
		return memoryCache.get(key);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.i(TAG, "getView: " + position + ", " + convertView);
		ViewHolder vh;

		// Open the individual photo view
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_view_photo, parent, false);
			vh = new ViewHolder();
			vh.imageView = convertView.findViewById(R.id.photo);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		vh.position = position;
		vh.imageView.setImageBitmap(null);

		new AsyncTask<ViewHolder, Void, Bitmap>() {
			private ViewHolder vh;

			@Override
			protected Bitmap doInBackground(ViewHolder... params) {
				vh = params[0];
				Bitmap bmp = null;
				try {
					if (vh.position != position) {
						return null;
					}

					// Get bitmap from cache, if it doesn't exist, add it
					bmp = getBitmapFromMemCache(imageList.get(position).filepath);
					if (bmp != null){
						return bmp;
					} else {
						bmp = BitmapFactory.decodeFile(imageList.get(position).filepath);
						bmp = ThumbnailUtils.extractThumbnail(bmp, 400, 400);
						addBitmapToMemoryCache(imageList.get(position).filepath, bmp);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				return bmp;
			}

			// Show the image
			@Override
			protected void onPostExecute(Bitmap bmp) {
				if (vh.position == position) {
					vh.imageView.setImageBitmap(bmp);
					vh.imageView.setRotation(imageList.get(position).orientation);
				}
			}
		}.execute(vh);

		return convertView;
	}
}
