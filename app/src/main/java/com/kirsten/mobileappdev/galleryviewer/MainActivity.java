package com.kirsten.mobileappdev.galleryviewer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.LruCache;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends Activity {
	String filePath;
	int orientation;
	ArrayList<ImageData> images = new ArrayList<>(); // Store a list of the data from all the images

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Create a cache of the thumbnails
		// Android Developer
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 8;
		RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getFragmentManager());
		LruCache<String, Bitmap> mMemoryCache = retainFragment.mRetainedCache;
		if (mMemoryCache == null) {
			mMemoryCache = new LruCache<String, Bitmap>(cacheSize){
				@Override
				protected int sizeOf(String key, Bitmap bmp){
					return bmp.getByteCount() / 1024;
				}
			};
			retainFragment.mRetainedCache = mMemoryCache;
		}

		GridView gridView = findViewById(R.id.gridview);
		gridView.setAdapter(new ImageAdapter(this, images, mMemoryCache));

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this ,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
		} else {
			ViewPhotos();
		}

		// Get the image data and add it to the array list
		Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.ORIENTATION};
		String sortBy = MediaStore.Images.Media.DATE_TAKEN + " DESC";
		Cursor cursor = getContentResolver().query(uri, projection, null, null, sortBy);
		while (cursor.moveToNext()){
			filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
			orientation = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.ORIENTATION));
			images.add(new ImageData(filePath, orientation));
		}
		cursor.close();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (requestCode == 1 && grantResults.length > 0) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				ViewPhotos();
			} else {
				finish();
			}
		}
	}

	// Open the individual photo view
	public void ViewPhotos(){
		GridView gridView = findViewById(R.id.gridview);
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(MainActivity.this, ViewPhoto.class);
				String photoPath = images.get(position).filepath;
				int orientation = images.get(position).orientation;
				intent.putExtra("PHOTO_PATH", photoPath);
				intent.putExtra("ORIENTATION", orientation);
				startActivity(intent);
			}
		});
	}
}
