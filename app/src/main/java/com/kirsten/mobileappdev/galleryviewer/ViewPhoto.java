package com.kirsten.mobileappdev.galleryviewer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class ViewPhoto extends Activity {
	// The individual photo view
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_photo);
		getActionBar().setDisplayHomeAsUpEnabled(true); // Shows the action bar

		ImageView imageView = findViewById(R.id.photo);
		String photoPath = getIntent().getStringExtra("PHOTO_PATH");
		int orientation = getIntent().getIntExtra("ORIENTATION", 0);
		Bitmap bmp = BitmapFactory.decodeFile(photoPath);
		imageView.setImageBitmap(bmp);
		imageView.setRotation(orientation);
	}
}
