package com.kirsten.mobileappdev.galleryviewer;

public class ImageData {
	// This class is used to get the path and rotation of the image
	String filepath;
	int orientation;

	public ImageData(String filepath, int orientation){
		this.filepath = filepath;
		this.orientation = orientation;
	}
}
